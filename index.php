<?php get_header();
$recent_posts_array = get_posts(); // получаем массив постов
?>
<div class="main">
    <div class="content">
        <div class="main-content">
            <h1><?php echo get_theme_mod('title_site', 'Добро пожаловать на официальный сайт храма преподобного Симеона Столпника на Поварской'); ?></h1>
            <p><img style="float: left; margin-left: 10px; margin-right: 10px;" src="<?php echo get_theme_mod( 'photo_site' ); ?>" alt="" width="330" height="267"><?php echo get_theme_mod('description_site', 'Добро'); ?></p>
        </div>
        <div class="news-content">
            <h1>Новости</h1>
            <hr>
            <?php?>
            <style>
                .news-img img{
                    height: auto;
                }
            </style>
            <?php
                while( have_posts() ) : the_post();
                echo'
                <div class="news-box">
                <span class="news-info">
                    <h2>'.get_the_title().'</h2>
                    <span class="date">'.get_the_date('j F Y').'</span>
                    <p class="text"><em>'.wp_trim_words(get_the_content(), 20, '...').'</em></p>
                    <a href="' . get_permalink() . '">Читать полностью</a>
                </span>
                    <span class="news-img" style = "img{height:auto}">';
                    echo the_post_thumbnail();
                    echo'</span>
                </div>
                ';  
                endwhile;
              
            ?> 
            <?php ?>

        </div>
    </div>
    <?php get_sidebar() ?>
    <?php get_footer() ?>
</body>
</html>