<?php get_header();
$recent_posts_array = get_posts(); // получаем массив постов
?>
<div class="main">
    <div class="content">
        <div id="primary" class="content-area">
		    <main id="main" class="site-main" role="main">
            <?php
		// Start the loop.
        while ( have_posts() ) : the_post();
            the_post();

			// Include the page content template.
            get_template_part( 'content', get_post_format() );
            echo the_content();

			// If comments are open or we have at least one comment, load up the comment template

		// End the loop.
		endwhile;
		?>
            </main>
        </div>
    </div>
<?php get_sidebar() ?>
<?php get_footer() ?>