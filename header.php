<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_enqueue_style( 'style', get_stylesheet_uri() ); ?>
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>
<body>
<style>
	.wrapper section{
		background: url('<?php get_theme_mod('title_site')?>') no-repeat top;
	}
</style>
<div class="wrapper">
    <!-- site-header -->
	<header class = 'site-header'>
		<section style = 'background: url("<?php echo get_theme_mod("background_photo")?>") no-repeat top; background-size: 100% 100%'></section>
		<nav class = 'site-nav'>
			<?php wp_nav_menu(); ?>
		</nav>
	</header><!-- /site-header -->
