<?php 
    function add_news_menu() {
        add_menu_page(
            'Новости',
            'Новости', 
            'manage_options', // уровень доступа
            'admin_news', // slug страницы
            'render_news_page', // функция, отображающая собственно страницу
            'dashicons-editor-help', // иконка
            '10' // позиция в меню
        );
    }
    add_action('admin_menu', 'add_news_menu');
?>