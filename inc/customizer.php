<?php
add_action('customize_register', function($customizer) {
    /* Настройки сайта */
    $customizer->add_section(
        'section_one', array(
            'title' => 'Настройки сайта',
            'description' => '',
            'priority' => 11,
        )
    );
    $customizer->add_setting('title_site', 
        array('default' => 'Добро пожаловать на официальный сайт храма преподобного Симеона Столпника на Поварской')
    );
    
    $customizer->add_control('title_site', array(
            'label' => 'Заголовок',
            'section' => 'section_one',
            'type' => 'text',
        )
    );
    $customizer->add_setting('description_site', 
        array('default' => '')
    );
    
    $customizer->add_control('description_site', array(
            'label' => 'Описание',
            'section' => 'section_one',
            'type' => 'textarea',
        )
    );
    $customizer->add_setting('photo_site', 
        array('default' => '')
    );
    $customizer->add_control(
        new WP_Customize_Image_Control(
            $customizer,
            'photo_site',
            array(
                'label'      => __( 'Изображение к описанию', 'church' ),
                'section'    => 'section_one',
                'settings'   => 'photo_site',
            )
        )
    );
    $customizer->add_setting('background_photo', 
        array('default' => '')
    );
    $customizer->add_control(
        new WP_Customize_Image_Control(
            $customizer,
            'background_photo',
            array(
                'label'      => __( 'Изображение к хэдеру', 'church' ),
                'section'    => 'section_one',
                'settings'   => 'background_photo',
            )
        )
    );
    $customizer->add_setting('calendar_photo', 
        array('default' => '')
    );
    $customizer->add_control(
        new WP_Customize_Image_Control(
            $customizer,
            'calendar_photo',
            array(
                'label'      => __( 'Изображение к календарю', 'church' ),
                'section'    => 'section_one',
                'settings'   => 'calendar_photo',
            )
        )
    );
    /* Настройка изображений */
});