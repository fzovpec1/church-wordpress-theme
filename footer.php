<footer>
    <div class="copyright">
        <p>
            Храм преподобного Симеона Столпника, 2014<br>
            © Все права защищены.<br>
            При использовании материалов сайта <br>
            ссылка на сайт <a href="www.st-simeon.ru">www.st-simeon.ru</a> обязательна.
        </p>

    </div>
</footer>