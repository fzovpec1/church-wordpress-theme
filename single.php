<?php get_header();?>
<div class="main">
    <div class="content">
        <div class="main-content">
            <h1 style = 'margin-top:10px;'><?php the_title(); ?></h1>
            <?php echo the_post_thumbnail('large'); ?>
            
            <p>
                <?php
                if ( have_posts() ) : while ( have_posts() ) : the_post();
                    the_content();
                endwhile;
                endif;
                wp_reset_postdata();
                ?>
            </p>
        </div>
    </div>
<?php get_sidebar(); ?>
<?php get_footer() ?>