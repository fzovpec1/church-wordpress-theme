<?php 
    include 'inc/admin.php';
    add_theme_support( 'post-thumbnails' );
    register_nav_menus(array(
        'top'    => 'Верхнее меню',    //Название месторасположения меню в шаблоне
    ));
    include 'inc/customizer.php';
?>